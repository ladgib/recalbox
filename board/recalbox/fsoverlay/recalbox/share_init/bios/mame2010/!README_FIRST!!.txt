This archive you are now viewing contains 3 files

!README_FIRST!!.txt   <--- the file you reading
cheat.txt  <--- A text file with credits and other info (WIP)           
cheat.zip  <--- The cheat file, DO NOT UNZIP THIS ZIP!!!


Under no circumstances should you unzip the cheat.zip contained in this 
archive, it contains 8831 xml files and it should always be kept zipped up.

First of all unzip/un7z the downloaded archive and copy the still packed
cheat.zip file into the directory that contains the MAME executeable
(All MAME versions apart from the non-SDL Mac OS 9/X MAME ). 
For MAME OS X (not SDL-MAME OSX) the cheat.zip file should be in:-
${HOME}/Library/Application Support/MAME OS X

Read the cheat.txt file for more info.

If you spot any errors (non-working cheats or parsing errors) then 
please post them in this topic:-

http://www.mamecheat.co.uk/forums/viewtopic.php?t=3263